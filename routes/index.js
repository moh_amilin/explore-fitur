var express = require('express');
var router = express.Router();
const {create, view, down}  = require('../controllers/topics')
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/topic', view)
router.get('/topic/down/:id', down)


module.exports = router;
