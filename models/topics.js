'use strict';

module.exports = (sequelize, DataTypes) => {
  const Topic = sequelize.define('topics', {
    topic: DataTypes.STRING,
    name_id: DataTypes.INTEGER,
    priority: DataTypes.INTEGER
  })
  return Topic;
};