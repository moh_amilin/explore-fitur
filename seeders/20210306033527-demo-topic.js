'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('topics', [
      {
        id: 1,
        topic: 'topic 1',
        name_id: 2,
        priority: 0,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 2,
        topic: 'topic 2',
        name_id: 2,
        priority: 0,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 3,
        topic: 'topic 3',
        name_id: 2,
        priority: 0,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 4,
        topic: 'topic 4',
        name_id: 2,
        priority: 0,
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {});
    /**
     * Add seed commands here.
     *
  
    */
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     */
    await queryInterface.bulkDelete('topics', null, {});
  }
};
